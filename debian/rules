#!/usr/bin/make -f
# -*- makefile -*-
#export DH_VERBOSE=1
export DESTDIR=$(CURDIR)/debian/tmp

# Set multiarch triplet
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

# Initialize empty flag variables and export them
export DEB_CFLAGS_MAINT_APPEND =
export DEB_CXXFLAGS_MAINT_APPEND =
export DEB_CPPFLAGS_MAINT_APPEND =

# Reduce size of debug symbols to fix FTBFS due to the
# 2GB/3GB address space limits on 32bit
DEB_HOST_ARCH_BITS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_BITS)
ifeq (32,$(DEB_HOST_ARCH_BITS))
  DEB_CFLAGS_MAINT_APPEND += -g1
  DEB_CXXFLAGS_MAINT_APPEND += -g1
endif

# minimise needless linking
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# Enable hardening
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

export DEB_HOST_ARCH      ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

export JAVA_HOME=/usr/lib/jvm/default-java
## Set JAVAC to prevent FTBFS due to incorrect use of 'gcj' if found (see "m4/ac_prog_javac.m4").
export JAVAC=javac

extraopts += -DWITH_OCF=ON
extraopts += -DWITH_DEBUG=ON
extraopts += -DWITH_CEPHFS_JAVA=ON
extraopts += -DCMAKE_INSTALL_LIBEXECDIR=/usr/lib

# rocksdb is not packaged by anyone.  build it if we can.
extraopts += -DWITH_SYSTEM_ROCKSDB=OFF

# Build Python 3 bindings
extraopts += -DWITH_PYTHON3=ON

# Use system boost
extraopts += -DWITH_SYSTEM_BOOST=ON

# Not all architectures support libboost-context and libboost-coroutine
# Disabling boost-context also disables the RADOS GW Beast frontend
ifneq (,$(filter $(DEB_HOST_ARCH), i386 amd64 armel armhf arm64 mips mipsel powerpc ppc64el))
  extraopts += -DWITH_BOOST_CONTEXT=ON
else
  extraopts += -DWITH_BOOST_CONTEXT=OFF
endif

# Setup max parallel builds based on architecture
ifeq ($(DEB_HOST_ARCH), arm64)
  NUMJOBS=1
else ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  NUMJOBS=$(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
else
  NUMJOBS=2
endif

# Set _FILE_OFFSET_BITS=64 globally. While this is set by the build system
# through an automake variable, configure does not know about this and thus
# fails when testing xfs.h on 32bit systems.
DEB_CPPFLAGS_MAINT_APPEND += -D_FILE_OFFSET_BITS=64

# Build fix for mips/mipsel
# The first two variables work around a compiler bug which leads to virtual
# memory exhaustion while compiling the testsuite. See Debian bug #849657 and
# https://gcc.gnu.org/bugzilla/show_bug.cgi?id=79030
ifneq (,$(filter $(DEB_HOST_ARCH), mips mipsel))
  DEB_CFLAGS_MAINT_APPEND += --param ggc-min-expand=5
  DEB_CXXFLAGS_MAINT_APPEND += --param ggc-min-expand=5
endif

%:
	dh $@ --buildsystem=cmake --with javahelper,python2,python3 --max-parallel=$(NUMJOBS)

override_dh_auto_configure:
	dh_auto_configure --buildsystem=cmake -- $(extraopts)

override_dh_install:
	# NOTE: ensure that any versioned erasure coding test code is dropped
	#       from the package install - package ships unversioned modules.
	rm -f $(DESTDIR)/usr/lib/*/ceph/erasure-code/libec_*.so.*
	find $(DESTDIR)/usr/lib/*/ceph/erasure-code -type l -delete || :
	# Remove RUNPATH from JNI libraries. I did not find a way to disable this
	# using the upstream build system. Pointers welcome.
	# The /usr/lib/*/ceph directory needs to be preserved to find libceph-common.
	chrpath --replace /usr/lib/$(DEB_HOST_MULTIARCH)/ceph $(DESTDIR)/usr/lib/*/libcephfs_jni.so.1.0.0
	dh_install

override_dh_installinit:
	dh_installinit -pceph-common --no-start --name=rbdmap
	# The /etc/init.d/ceph SysV init script is already installed by the upstream
	# build system.
	dh_installinit -pceph-base --no-start --name=ceph --only-scripts
	dh_installinit --no-start --remaining
	# install the systemd stuff manually since we have funny service names
	# and need to update the paths in all of the files post install
	# systemd:ceph-common
	install -d -m0755 debian/ceph-common/usr/lib/tmpfiles.d
	install -m 0644 -D systemd/ceph.tmpfiles.d debian/ceph-common/usr/lib/tmpfiles.d/ceph.conf
	install -d -m0755 debian/ceph-common/lib/systemd/system
	install -m 0644 systemd/rbdmap.service debian/ceph-common/lib/systemd/system
	# systemd:ceph-base
	install -d -m0755 debian/ceph-base/lib/systemd/system
	install -m0644 systemd/ceph.target debian/ceph-common/lib/systemd/system
	# systemd:ceph-mon
	install -d -m0755 debian/ceph-mon/lib/systemd/system
	install -m0644 systemd/ceph-mon@.service debian/ceph-mon/lib/systemd/system
	sed -i s./etc/sysconfig/./etc/default/.g debian/ceph-mon/lib/systemd/system/ceph-mon@.service
	install -m0644 systemd/ceph-mon.target debian/ceph-mon/lib/systemd/system
	# systemd:ceph-osd
	install -d -m0755 debian/ceph-osd/lib/systemd/system
	install -m0644 systemd/ceph-osd@.service debian/ceph-osd/lib/systemd/system
	install -m0644 systemd/ceph-disk@.service debian/ceph-osd/lib/systemd/system
	sed -i s./etc/sysconfig/./etc/default/.g debian/ceph-osd/lib/systemd/system/ceph-osd@.service
	sed -i s./etc/sysconfig/./etc/default/.g debian/ceph-osd/lib/systemd/system/ceph-disk@.service
	install -m0644 systemd/ceph-osd.target debian/ceph-osd/lib/systemd/system
	# systemd:ceph-mds
	install -d -m0755 debian/ceph-mds/lib/systemd/system
	install -m0644 systemd/ceph-mds@.service debian/ceph-mds/lib/systemd/system
	sed -i s./etc/sysconfig/./etc/default/.g debian/ceph-mds/lib/systemd/system/ceph-mds@.service
	install -m0644 systemd/ceph-mds.target debian/ceph-mds/lib/systemd/system
	# systemd:radosgw
	install -d -m0755 debian/radosgw/lib/systemd/system
	install -m0644 systemd/ceph-radosgw@.service debian/radosgw/lib/systemd/system
	sed -i s./etc/sysconfig/./etc/default/.g debian/radosgw/lib/systemd/system/ceph-radosgw@.service
	install -m0644 systemd/ceph-radosgw.target debian/radosgw/lib/systemd/system
	# disable SysV init file for systemd
	ln -s /dev/null debian/radosgw/lib/systemd/system/radosgw.service
	# systemd:rbd-mirror
	install -d -m0755 debian/rbd-mirror/lib/systemd/system
	install -m0644 systemd/ceph-rbd-mirror@.service debian/rbd-mirror/lib/systemd/system
	sed -i s./etc/sysconfig/./etc/default/.g debian/rbd-mirror/lib/systemd/system/ceph-rbd-mirror@.service
	install -m0644 systemd/ceph-rbd-mirror.target debian/rbd-mirror/lib/systemd/system
	# systemd:ceph-fuse
	install -d -m0755 debian/ceph-fuse/lib/systemd/system
	install -m0644 systemd/ceph-fuse@.service debian/ceph-fuse/lib/systemd/system
	sed -i s./etc/sysconfig/./etc/default/.g debian/ceph-fuse/lib/systemd/system/ceph-fuse@.service
	install -m0644 systemd/ceph-fuse.target debian/ceph-fuse/lib/systemd/system

	# Ensure Debian/Ubuntu specific systemd units are NOT automatically enabled and started
	# Enable systemd targets only
	dh_systemd_enable -Xceph-mon.service -Xceph-osd.service -X ceph-mds.service
	# Start systemd targets only
	dh_systemd_start --no-restart-on-upgrade -Xceph-mon.service -Xceph-osd.service -X ceph-mds.service

override_dh_systemd_enable:
	# systemd enable done as part of dh_installinit

override_dh_systemd_start:
	# systemd start done as part of dh_installinit

override_dh_makeshlibs:
	# exclude jni libraries in libcephfs-jni to avoid pointless ldconfig
	# calls in maintainer scripts; exclude private erasure-code plugins.
	dh_makeshlibs -V -X/usr/lib/jni -X/usr/lib/$(DEB_HOST_MULTIARCH)/ceph/erasure-code \
		-X/usr/lib/$(DEB_HOST_MULTIARCH)/ceph/compressor \
		-X/usr/lib/$(DEB_HOST_MULTIARCH)/rados-classes \

override_dh_auto_test:
	-dh_auto_test -v || cat src/test-suite.log

# can be removed once migrated to debhelper compat level 12 where --list-missing is
# the default
override_dh_missing:
	dh_missing --list-missing

override_dh_clean:
	# While these files look silly they are really part of the upstream source
	# tarball and should thus be kept until upstream fixes this.
	dh_clean --exclude=src/civetweb/examples/websocket_client/ssl/server.key.orig \
	         --exclude=src/civetweb/resources/cert/client.key.orig \
	         --exclude=src/civetweb/resources/cert/server.key.orig \
	         --exclude=src/erasure-code/jerasure/jerasure/Examples/makefile.orig \
	         --exclude=src/erasure-code/jerasure/jerasure/include/config.h.in~ \
	         --exclude=src/erasure-code/jerasure/jerasure/makefile.orig \
	         etc/sysctl/90-ceph-osd.conf

.PHONY: override_dh_auto_configure override_dh_installinit override_dh_makeshlibs override_dh_auto_test override_dh_missing
